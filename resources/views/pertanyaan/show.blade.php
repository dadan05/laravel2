@extends('layout.master')

@section('header-content')
    <h1>Detail</h1>
@endsection

@section('content')
    <table>
        <tr>
            <th>Judul</th>
            <td>{{$post -> judul}}</td>
        </tr>
        <tr>
            <th>Isi</th>
            <td>{{$post -> isi}}</td>
        </tr>
        <tr>
            <th>Tanggal dibuat</th>
            <td>{{date("d-m-Y", strtotime($post->tanggal_dibuat))}}</td>
        </tr>
        <tr>
            <th>Tanggal diperbaharui</th>
            <td>{{date("d-m-Y", strtotime($post->tanggal_diperbaharui))}}</td>
        </tr>
    </table>
    <a href="/pertanyaan" class="btn btn-sm btn-primary mt-3">back</a>
@endsection