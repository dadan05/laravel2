@extends('layout.master')

@section('header-content')
    <h1>PERTANYAAN</h1>
@endsection


@section('content')
<div class="card">
        <div class="card-header">
            @if(session('success'))
                <div class="alert alert-success">
                    {{session('success')}}
                </div>
            @endif
            <a href="/pertanyaan/create" class="btn btn-sm btn-primary">Tambah Data</a>
        </div>
        <!-- /.card-header -->
        <div class="card-body">
            <table id="example1" class="table table-bordered table-striped">
            <thead>
            <tr>
                <th>No.</th>
                <th>Judul</th>
                <th>Isi</th>
                <th>Tanggal buat</th>
                <th>Tanggal diperbaharui</th>
                <th style="width:40px">Action</th>
            </tr>
            </thead>
            <tbody>
            <tr>
            @foreach($posts as $key => $post)
                <td> {{$key + 1}} </td>
                <td> {{$post -> judul}} </td>
                <td> {{$post -> isi}} </td>
                <td> {{$post -> tanggal_dibuat}} </td>
                <td> {{$post -> tanggal_diperbaharui}} </td>
                <td style="display:flex">
                    <a href="/pertanyaan/{{$post->id}}" class="btn btn-sm btn-info">show</a>
                    <a href="/pertanyaan/{{$post->id}}/edit" class="btn btn-sm btn-default ml-1">edit</a>
                    <form action="/pertanyaan/{{$post ->id}}" method="POST">
                        @csrf
                        @method('DELETE')
                        <input type="submit" class="btn btn-sm btn-danger ml-1" value="delete">
                    </form>
                </td>
            @endforeach
            </tr>
            </table>
        </div>
        <!-- /.card-body -->
</div>
@endsection

@push('scripts')
    <script src="{{asset('adminlte/plugins/datatables/jquery.dataTables.js')}}"></script>
    <script src="{{asset('adminlte/plugins/datatables-bs4/js/dataTables.bootstrap4.js')}}"></script>
    <script>
    $(function () {
        $("#example1").DataTable();
    });
    </script>
@endpush